#!/usr/bin/env python
# Chess by gordon hennesy
# chess board graphics by http://opengameart.org/content/chessboard
# (needed to be flipped left-to-right)
# User: twin_mice
# License: Public Domain
# chess piece graphics by http://opengameart.org/content/chess-pieces
# (needed to switch king and queen graphic files)
# User: Anonymous
# License: Public Domain
import pygame
from pygame.locals import *
from pygame.sprite import *
import sys
import math
import random
board = []
width = 70
height = 69
piece_values={'pawn':1,'rook':2,'bishop':3,'knight':4,'queen':5,'king':6}
class ChessPiece(pygame.sprite.Sprite):
   def __init__(self, image, color, piece, left, top, loc, flags={}):
      Sprite.__init__(self)
      self.image = image.convert()
      self.image.set_colorkey((255, 255, 255))
      self.rect = self.image.get_rect()
      self.rect.topleft = (left, top)
      self.color = color
      self.piece = piece
      self.currentLocation = loc
      self.prevLocation = loc
      self.value = piece_values[piece]
      self.one_time_flags = flags
      if piece == 'pawn':
         self.one_time_flags ={'first_move':True, 'enpassant_vulnerable':False}
      elif piece == 'king':
         self.one_time_flags ={'first_move':True, 'enpassant_vulnerable':False}
      elif piece == 'rook':
         self.one_time_flags ={'first_move':True, 'enpassant_vulnerable':False}

   def __str__(self):
      return self.piece + ' at ' + str(self.currentLocation)

class ChessBoardSquare(pygame.sprite.Sprite):
   def __init__(self, color, abbrev, left, top):
      "a8 white"
      Sprite.__init__(self)
      self.image = pygame.Surface((width,height))
      self.image.set_colorkey((255, 255, 255))
      self.rect = self.image.get_rect()
      self.rect.topleft = (left, top)
      self.color = color
      self.abbrev = abbrev
class Cursor(pygame.sprite.Sprite):
   def __init__(self):
      Sprite.__init__(self)
      self.rect = pygame.rect.Rect(0,0,1,1)
      self.sprite_to_drag = None
   def update(self):
      "move the (invisible) cursor based on the mouse position"
      pos = pygame.mouse.get_pos()
      self.rect.center = pos
      if self.sprite_to_drag:
         self.sprite_to_drag.rect.center = self.rect.center   
def move(loc, loc_to):
   board[loc_to[0]][loc_to[1]] = board[loc[0]][loc[1]]
   board[loc[0]][loc[1]] = 0
def print_board():
   for row in board:
      print row
def move_prompt():
   mov = input("Move?")
   the_move = mov[0],mov[1]
   the_move = mov[2],mov[3]
   move( ( mov[0],mov[1]), ( mov[2],mov[3]))   
def letter_before(letter):
   if letter == 'a' or letter == None:
      return None
   else:
      return chr(ord(letter)-1)
def letter_after(letter):
   if letter == 'h' or letter == None:
      return None
   else:
      return chr(ord(letter)+1)
class Chess():
   def __init__(self):
      self.screen = pygame.display.set_mode((600,600))
      row = [4,1,2,5,6,2,3,4]
      board.append(row)
      row = [1,1,1,1,1,1,1,1]
      board.append(row)
      row = [0,0,0,0,0,0,0,0]
      board.append(row)
      row = [0,0,0,0,0,0,0,0]
      board.append(row)
      row = [0,0,0,0,0,0,0,0]
      board.append(row)
      row = [0,0,0,0,0,0,0,0]
      board.append(row)
      row = [1,1,1,1,1,1,1,1]
      board.append(row)
      row = [4,1,2,5,6,2,3,4]
      board.append(row)
      
      self.pieces = pygame.sprite.RenderUpdates()
      self.squares = pygame.sprite.RenderUpdates()

      leftborder = 31
      topborder = 31
      left = leftborder+0
      top = topborder + 0
      
      self.chess_piece_2_black_pawn = pygame.image.load("chess_piece_2_black_pawn.png")
      self.chess_piece_2_black_rook = pygame.image.load("chess_piece_2_black_rook.png")
      self.chess_piece_2_black_knight = pygame.image.load("chess_piece_2_black_knight.png")
      self.chess_piece_2_black_bishop = pygame.image.load("chess_piece_2_black_bishop.png")
      self.chess_piece_2_black_king = pygame.image.load("chess_piece_2_black_king.png")
      self.chess_piece_2_black_queen = pygame.image.load("chess_piece_2_black_queen.png")

      self.chess_piece_2_white_pawn = pygame.image.load("chess_piece_2_white_pawn.png")
      self.chess_piece_2_white_rook = pygame.image.load("chess_piece_2_white_rook.png")
      self.chess_piece_2_white_knight = pygame.image.load("chess_piece_2_white_knight.png")
      self.chess_piece_2_white_bishop = pygame.image.load("chess_piece_2_white_bishop.png")
      self.chess_piece_2_white_king = pygame.image.load("chess_piece_2_white_king.png")
      self.chess_piece_2_white_queen = pygame.image.load("chess_piece_2_white_queen.png")

      self.pieces.add(ChessPiece(self.chess_piece_2_black_rook, 'black', 'rook', leftborder+0, topborder+0, 'a8'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_knight, 'black','knight', leftborder+1*width, topborder + 0,'b8'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_bishop, 'black', 'bishop',leftborder+2*width,topborder + 0,'c8'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_queen, 'black', 'queen',leftborder+3*width,topborder + 0,'d8'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_king, 'black', 'king',leftborder+4*width,topborder + 0,'e8'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_bishop, 'black', 'bishop',leftborder+5*width,topborder + 0,'f8'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_knight, 'black', 'knight',leftborder+6*width,topborder + 0,'g8'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_rook, 'black', 'rook',leftborder+7*width,topborder + 0,'h8'))

      self.pieces.add(ChessPiece(self.chess_piece_2_black_pawn, 'black', 'pawn', leftborder+0*width,topborder + 1*height,'a7'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_pawn, 'black', 'pawn', leftborder+1*width,topborder + 1*height,'b7'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_pawn, 'black', 'pawn', leftborder+2*width,topborder + 1*height,'c7'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_pawn, 'black', 'pawn', leftborder+3*width,topborder + 1*height,'d7'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_pawn, 'black', 'pawn', leftborder+4*width,topborder + 1*height,'e7'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_pawn, 'black', 'pawn', leftborder+5*width,topborder + 1*height,'f7'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_pawn, 'black', 'pawn', leftborder+6*width,topborder + 1*height,'g7'))
      self.pieces.add(ChessPiece(self.chess_piece_2_black_pawn, 'black', 'pawn', leftborder+7*width,topborder + 1*height,'h7'))

      self.pieces.add(ChessPiece(self.chess_piece_2_white_rook, 'white', 'rook', leftborder+0, topborder+7*height, 'a1'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_knight, 'white','knight', leftborder+1*width, topborder + 7*height,'b1'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_bishop, 'white', 'bishop',leftborder+2*width,topborder + 7*height,'c1'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_queen, 'white', 'queen',leftborder+3*width,topborder + 7*height,'d1'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_king, 'white', 'king',leftborder+4*width,topborder + 7*height,'e1', {'castle_k': True, 'castle_q': True}))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_bishop, 'white', 'bishop',leftborder+5*width,topborder + 7*height,'f1'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_knight, 'white', 'knight',leftborder+6*width,topborder + 7*height,'g1'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_rook, 'white', 'rook',leftborder+7*width,topborder + 7*height,'h1'))

      self.pieces.add(ChessPiece(self.chess_piece_2_white_pawn, 'white', 'pawn', leftborder+0*width,topborder + 6*height,'a2'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_pawn, 'white', 'pawn', leftborder+1*width,topborder + 6*height,'b2'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_pawn, 'white', 'pawn', leftborder+2*width,topborder + 6*height,'c2'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_pawn, 'white', 'pawn', leftborder+3*width,topborder + 6*height,'d2'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_pawn, 'white', 'pawn', leftborder+4*width,topborder + 6*height,'e2'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_pawn, 'white', 'pawn', leftborder+5*width,topborder + 6*height,'f2'))
      self.pieces.add(ChessPiece(self.chess_piece_2_white_pawn, 'white', 'pawn', leftborder+6*width,topborder + 6*height,'g2'))
      chess_piece=ChessPiece(self.chess_piece_2_white_pawn, 'white', 'pawn', leftborder+7*width,topborder + 6*height,'h2')
      self.pieces.add(chess_piece)
      
      self.game_not_over = True
      self.board = pygame.image.load("chessboard.png")
      self.board.set_colorkey()
      board_width=600
      board_height=600
      self.board = pygame.transform.scale(self.board, (board_width, board_height))
      #self.board = pygame.transform.rotate(self.board, 90.0)
      self.board = pygame.transform.flip(self.board, True, False)
      self.cursor = Cursor()
      index = 0
      y = 0
      for rank in range(8,0,-1):
         x = 0
         for alph in ['a','b','c','d','e','f','g','h']:
            if index % 2 == 0:
               color = 'white'
            else:
               color = 'black'
            index += 1
            abbrev = alph + str(rank)
            self.squares.add(ChessBoardSquare(color, abbrev, leftborder+x*width, topborder+y*height))
            x += 1
            print abbrev,
         print
         y += 1
      #self.piece_move(chess_piece, 'h4')
      #for sq in self.squares:
      #   print sq.abbrev
   def piece_move(self, chess_piece, to_location):
      square_to_move_to = self.square_at_location(to_location)
      if square_to_move_to:
         chess_piece.prevLocation = chess_piece.currentLocation
         chess_piece.currentLocation = to_location
         chess_piece.rect.topleft = square_to_move_to.rect.topleft
         if chess_piece.piece == 'pawn':
            prevrank = chess_piece.prevLocation[1]
            currrank = chess_piece.currentLocation[1]
            if int(math.floor(math.fabs(int(prevrank) - int(currrank)))) == 2:
               chess_piece.one_time_flags['first_move'] = False
               chess_piece.one_time_flags['enpassant_vulnerable'] = True
            elif chess_piece.one_time_flags['enpassant_vulnerable']:
               chess_piece.one_time_flags['enpassant_vulnerable'] = False
               
   def square_at_location(self, location):
      target_square = None
      for sq in self.squares:
         if sq.abbrev == location:
            target_square = sq
      if target_square == None:
         print 'Error, no square found at location',location
      return target_square
   def piece_at_location(self, location, color=None):
      square = self.square_at_location(location)
      pieces_at_location = pygame.sprite.spritecollide(square, self.pieces, False)
      if len(pieces_at_location) > 1:
         print 'Error: location has more than 1 piece (%s)' % color,location,len(pieces_at_location)
         for i in range(len(pieces_at_location)):
            apiece = pieces_at_location[i]
            if color:
               if apiece.color == color:
                  return apiece
            else:
               return apiece
      elif len(pieces_at_location) > 0:
         return pieces_at_location[0]
      else:
         #print "Error: no pieces found at location",location
         pass
      return None

   def make_move(self, from_location, to_location, piece_to_capture, move_number, color):
      piece_to_move = self.piece_at_location(from_location,color)
      #if len(piece_to_move
      #if self.cursor.sprite_to_drag:
      #   if self.cursor.sprite_to_drag != piece_to_move:
      #      return
      if piece_to_move:
         self.piece_move(piece_to_move, to_location)
      if piece_to_capture:
         self.pieces.remove(piece_to_capture)
      pass
   def check_move(self, chess_piece, to_location, move_number):
      "1.e4 e5 (also called a Double King's Pawn Opening or Double King's Pawn Game)"
      #print 'info',chess_piece.currentLocation, to_location.abbrev
      
      # move the piece back, in order to start the move from the original
      # location
      self.piece_move(chess_piece, chess_piece.currentLocation)

      move_ok = False
      for mov in self.pieces_move(chess_piece):
         loc, newloc = mov[1], mov[2]
         #print newloc, to_location.abbrev
         if to_location.abbrev == newloc:
            move_ok = True
      if move_ok and chess_piece.color == 'white':
         piece_to_capture = self.piece_at_location(to_location.abbrev, self.opponent_color(chess_piece.color))
         #print 'Capture:', str(piece_to_capture)
         self.make_move(chess_piece.currentLocation, to_location.abbrev, piece_to_capture, move_number, chess_piece.color)
         print str(move_number)+'.',chess_piece.piece, chess_piece.currentLocation, to_location.abbrev,
         return True
      else:
         #print 'NO, you cannot move there, dumbass!'
         self.piece_move(chess_piece, chess_piece.currentLocation)
         
      return False
      
   def last_rank(self, color):
      if color == 'white':
         return 8
      else:
         return 1
   def pawn_dir(self, color):
      if color == 'white':
         return 1
      else:
         return -1
   def opponent_color(self, color):
      if color == 'white':
         return 'black'
      else:
         return 'white'
   def knight_moves(self, loc, color):
      moves = []
      newlocs = []
      letter = loc[0]
      number = int(loc[1])
      index = number + 1
      let = letter_after(letter_after(letter))
      if let != None and index <= 8:
         newloc = let+str(index)
         newlocs.append(newloc)
      let = letter_before(letter_before(letter))
      if let != None and index <= 8:
         newloc = let+str(index)
         newlocs.append(newloc)
      index = number + 2
      let = letter_after(letter)
      if let != None and index <= 8:
         newloc = let+str(index)
         newlocs.append(newloc)
      let = letter_before(letter)
      if let != None and index <= 8:
         newloc = let+str(index)
         newlocs.append(newloc)
      index = number - 1
      let = letter_after(letter_after(letter))
      if let != None and index >= 1:
         newloc = let+str(index)
         newlocs.append(newloc)
      let = letter_before(letter_before(letter))
      if let != None and index >= 1:
         newloc = let+str(index)
         newlocs.append(newloc)
      index = number - 2
      let = letter_after(letter)
      if let != None and index >= 1:
         newloc = let+str(index)
         newlocs.append(newloc)
      let = letter_before(letter)
      if let != None and index >= 1:
         newloc = let+str(index)
         newlocs.append(newloc)
      for newloc in newlocs:
         piece_to_capture = self.piece_at_location(newloc)
         if piece_to_capture == None:
            value = 1
            moves.append([value, loc, newloc, piece_to_capture])
         elif piece_to_capture.color == self.opponent_color(color):
            value = 1 + piece_to_capture.value
            moves.append([value, loc, newloc, piece_to_capture])
      return moves
   def rook_moves(self, loc, color):
      moves = []
      letter = loc[0]
      number = int(loc[1])
      index = number + 1
      let = letter
      stop = False
      while index <= 8 and let != None and stop == False:
         newloc = let + str(index)
         piece_to_capture = self.piece_at_location(newloc)
         if piece_to_capture == None:
            value = 1
            moves.append([value, loc, newloc, piece_to_capture])
         elif piece_to_capture.color == self.opponent_color(color):
            value = 1 + piece_to_capture.value
            moves.append([value, loc, newloc, piece_to_capture])
            stop = True
         elif piece_to_capture.color == color:
            stop = True
         index += 1         
      index = number - 1
      let = letter
      stop = False
      while index >= 1 and stop == False:
         newloc = let + str(index)
         piece_to_capture = self.piece_at_location(newloc)
         if piece_to_capture == None:
            value = 1
            moves.append([value, loc, newloc, piece_to_capture])
         elif piece_to_capture.color == self.opponent_color(color):
            value = 1 + piece_to_capture.value
            moves.append([value, loc, newloc, piece_to_capture])
            stop = True
         elif piece_to_capture.color == color:
            stop = True
         index -= 1         
      index = number
      let = letter_after(letter)
      stop = False
      while let != None and stop == False:
         newloc = let + str(index)
         piece_to_capture = self.piece_at_location(newloc)
         if piece_to_capture == None:
            value = 1
            moves.append([value, loc, newloc, piece_to_capture])
         elif piece_to_capture.color == self.opponent_color(color):
            value = 1 + piece_to_capture.value
            moves.append([value, loc, newloc, piece_to_capture])
            stop = True
         elif piece_to_capture.color == color:
            stop = True
         let = letter_after(let)
      index = number
      let = letter_before(letter)
      stop = False
      while let != None and stop == False:
         newloc = let + str(index)
         piece_to_capture = self.piece_at_location(newloc)
         if piece_to_capture == None:
            value = 1
            moves.append([value, loc, newloc, piece_to_capture])
         elif piece_to_capture.color == self.opponent_color(color):
            value = 1 + piece_to_capture.value
            moves.append([value, loc, newloc, piece_to_capture])
            stop = True
         elif piece_to_capture.color == color:
            stop = True
         let = letter_before(let)
      return moves
   def eval_newloc(self, loc, newloc, color):
      piece_to_capture = self.piece_at_location(newloc)
      stop = False
      move = []
      if piece_to_capture == None:
         value = 1
         move = [value, loc, newloc, piece_to_capture]
      elif piece_to_capture.color == self.opponent_color(color):
         value = 1 + piece_to_capture.value
         move = [value, loc, newloc, piece_to_capture]
         stop = True
      elif piece_to_capture.color == color:
         stop = True
      return stop, move
   def bishop_moves(self, loc, color):
      moves = []
      letter = loc[0]
      number = int(loc[1])
      index = number + 1
      let = letter_after(letter)
      stop = False
      while index <= 8 and let != None and stop == False:
         newloc = let + str(index)
         stop, move = self.eval_newloc(loc, newloc, color)
         #piece_to_capture = self.piece_at_location(newloc)
         #if piece_to_capture == None:
         #   value = 1
         #   moves.append([value, loc, newloc, piece_to_capture])
         #elif piece_to_capture.color == self.opponent_color(color):
         #   value = 1 + piece_to_capture.value
         #   moves.append([value, loc, newloc, piece_to_capture])
         #   stop = True
         #elif piece_to_capture.color == color:
         #   stop = True
         if move: moves.append(move)
         index += 1
         let = letter_after(let)
      index = number - 1
      let = letter_after(letter)
      stop = False
      while index >= 1 and let != None and stop == False:
         newloc = let + str(index)
         stop, move = self.eval_newloc(loc, newloc, color)
# 
#          piece_to_capture = self.piece_at_location(newloc)
#          if piece_to_capture == None:
#             value = 1
#             moves.append([value, loc, newloc, piece_to_capture])
#          elif piece_to_capture.color == self.opponent_color(color):
#             value = 1 + piece_to_capture.value
#             moves.append([value, loc, newloc, piece_to_capture])
#             stop = True
#          elif piece_to_capture.color == color:
#             stop = True
         if move: moves.append(move)
         index -= 1
         let = letter_after(let)
      index = number - 1
      let = letter_before(letter)
      stop = False
      while index >= 1 and let != None and stop == False:
         newloc = let + str(index)
         stop, move = self.eval_newloc(loc, newloc, color)
#          piece_to_capture = self.piece_at_location(newloc)
#          if piece_to_capture == None:
#             value = 1
#             moves.append([value, loc, newloc, piece_to_capture])
#          elif piece_to_capture.color == self.opponent_color(color):
#             value = 1 + piece_to_capture.value
#             moves.append([value, loc, newloc, piece_to_capture])
#             stop = True
#          elif piece_to_capture.color == color:
#             stop = True
         if move: moves.append(move)
         index -= 1
         let = letter_before(let)
      index = number + 1
      let = letter_before(letter)
      stop = False
      while index <= 8 and let != None and stop == False:
         newloc = let + str(index)
         stop, move = self.eval_newloc(loc, newloc, color)
#          piece_to_capture = self.piece_at_location(newloc)
#          if piece_to_capture == None:
#             value = 1
#             moves.append([value, loc, newloc, piece_to_capture])
#          elif piece_to_capture.color == self.opponent_color(color):
#             value = 1 + piece_to_capture.value
#             moves.append([value, loc, newloc, piece_to_capture])
#             stop = True
#          elif piece_to_capture.color == color:
#             stop = True
         if move: moves.append(move)
         index += 1
         let = letter_before(let)
         
      return moves      
   def queen_moves(self, loc, color):
      moves = self.bishop_moves(loc, color)
      moves.extend(self.rook_moves(loc, color))
      return moves      
   def king_moves(self, loc, color):
      moves = []
      letter = loc[0]
      number = int(loc[1])
      letters = letter_before(letter), letter, letter_after(letter)
      for index in range(number - 1, number + 2):
         if index <= 8 and index >= 1:
            newlocs = [let+str(index) 
               for let in letters
                  if let != None]
            for newloc in newlocs:
               piece_to_capture = self.piece_at_location(newloc)
               if piece_to_capture == None:
                  value = 1
                  moves.append([value, loc, newloc, piece_to_capture])
               elif piece_to_capture.color == self.opponent_color(color) and letter != let:
                  value = 1 + piece_to_capture.value
                  moves.append([value, loc, newloc, piece_to_capture])
               else:
                  pass
      #print moves
      #sys.exit()
      return moves
      #moves = self.pawn_moves(loc, color) 
      #moves = moves.extend(self.pawn_moves(loc, self.opponent_color(color)) 
      #return self.pawn_moves(loc, color)
      # add something to remove moves that are moving into check
   def pawn_moves(self, loc, color):
      # missing:
      # double square on the first move
      # en passant - yikes!
      # also final rank - turning into queen (or other pieces)
      moves = []
      letter = loc[0]
      number = int(loc[1])
      letters = letter_before(letter), letter, letter_after(letter)
      newlocs = [let+str(number+self.pawn_dir(color)) 
         for let in letters
            if let != None]
      if self.piece_at_location(loc).one_time_flags['first_move']:
         if self.piece_at_location(letter+str(number + self.pawn_dir(color))) == None:
            newlocs.append(letter+str(number + self.pawn_dir(color) + self.pawn_dir(color)))
      for newloc in newlocs:
         let = newloc[0]
         if number == self.last_rank(color):
            pass
         else:                  
            piece_to_capture = self.piece_at_location(newloc)
            if piece_to_capture == None:
               value = 1
               if letter == let:
                  moves.append([value, loc, newloc, piece_to_capture])
            elif piece_to_capture.color == self.opponent_color(color) and letter != let:
               if letter != let:
                  value = 1 + piece_to_capture.value
                  moves.append([value, loc, newloc, piece_to_capture])
            else:
               pass
      #print "PAWNEEEEEEEEE", moves
      return moves
   def pieces_move(self, piece):
      moves = []
      loc=piece.currentLocation
      letter = loc[0]
      number = int(loc[1])
      if piece.piece == 'pawn':
         pawn_moves = self.pawn_moves(loc, piece.color)
         if pawn_moves:
            #moves.extend(pawn_moves)
            moves = pawn_moves
      elif piece.piece == 'knight':
         knight_moves = self.knight_moves(loc, piece.color)
         if knight_moves:
            moves.extend(knight_moves)
      elif piece.piece == 'rook':
         rook_moves = self.rook_moves(loc, piece.color)
         if rook_moves:
            moves.extend(rook_moves)
      elif piece.piece == 'bishop':
         bishop_moves = self.bishop_moves(loc, piece.color)
         if bishop_moves:
            moves.extend(bishop_moves)
      elif piece.piece == 'king':
         king_moves = self.king_moves(loc, piece.color)
         if king_moves:
            moves.extend(king_moves)
      elif piece.piece == 'queen':
         queen_moves = self.queen_moves(loc, piece.color)
         if queen_moves:
            moves.extend(queen_moves)
      return moves
   def select_move(self, moves):
      random.shuffle(moves)
      #print moves
      moves.sort()
      #print moves
      
      move = moves[-1]
      val = move[0]
      movs = []
      for mov in moves:
         if mov[0] == val:
            movs.append(mov)
      random.shuffle(movs)
      mov = movs[-1]
      return move
   def computer_move(self, move_number, color):
      #moves = [self.pieces_move(piece) 
      #   for piece in self.pieces 
      #      if piece.color == color and self.pieces_move(piece) != []]
      moves = []
      for piece in self.pieces:
         if piece.color == color and self.pieces_move(piece) != []:
            moves.extend(self.pieces_move(piece))
      if (len(moves) == 0):
         print "Stalemate; I have no moves left!"
      else:      
         move = moves[-1]
         move = self.select_move(moves)
         piece_to_capture = move[3]
         self.make_move(move[1], move[2], piece_to_capture, move_number, color)
         print move[1], move[2]
   def game_loop(self):
      self.screen.blit(self.board,(0,0))
      move_number = 1
      while self.game_not_over:
         self.pieces.update()
         for event in pygame.event.get():
            if event.type == pygame.constants.KEYDOWN:
               if event.key == pygame.K_ESCAPE:
                  self.game_not_over = False
                  return
               elif event.key == pygame.K_m:
                  self.computer_move(move_number, 'black')
            elif event.type == pygame.constants.QUIT:
               self.game_not_over = False
               return
            elif event.type == pygame.constants.MOUSEBUTTONDOWN:
               #print event.pos, button
               #print self.cursor.rect
               pieces_clicked_on = pygame.sprite.spritecollide(self.cursor, self.pieces, False)
               if len(pieces_clicked_on) > 0:
                  self.cursor.sprite_to_drag = pieces_clicked_on[0]
            elif event.type == pygame.constants.MOUSEBUTTONUP:
               squares_clicked_on = pygame.sprite.spritecollide(self.cursor, self.squares, False)
               if len(squares_clicked_on) > 0:
                  if self.cursor.sprite_to_drag != None:
                     if self.cursor.sprite_to_drag.currentLocation == squares_clicked_on[0].abbrev:
                        pass
                     else:
                        if self.check_move(self.cursor.sprite_to_drag, squares_clicked_on[0], move_number):
                           self.computer_move(move_number, 'black')
                           move_number += 1
               self.cursor.sprite_to_drag = None
            elif event.type == pygame.constants.MOUSEMOTION:
               pass

         self.display_board()
   def display_board(self):
      self.pieces.clear(self.screen, self.board)
      self.pieces.update()
      self.pieces.draw(self.screen)
      self.cursor.update()
      pygame.display.flip()

def main():
   c = Chess()
   c.game_loop()
if __name__=='__main__':
   main()